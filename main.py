from flask import Flask, redirect, url_for, render_template

app = Flask(__name__)
app.config.update(ENV="development", DEBUG=True)


@app.route("/")
def root():
    return render_template("index.html")


@app.route("/<username>")
def user(username):
    return render_template("index.html", username=username)


@app.route("/admin")
def admin():
    return redirect(url_for("user", username="fake admin"))


@app.route("/ionic")
def ionic():
    return render_template("ionic.html")


@app.errorhandler(404)
def page_not_found(e):
    return redirect(url_for("root"))


if __name__ == "__main__":
    app.run()
