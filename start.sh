#!/bin/bash -e

python3 -m venv .venv
source .venv/bin/activate
which python
sleep 2
pip install -r requirements.txt
